import restify from "restify";
import mongoose from "mongoose";
import discover from "./src/Interfaces/Http/discover";
import DBConnection from "./config/database"; 

const server = restify.createServer({
	name: "auth-service",
	ignoreTrailingSlash: true
});

server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser());

/**
 * Connect to MongoDB server
 */
DBConnection();

const router = require("./src/Interfaces/Http/Routes/index");
router.applyRoutes(server);

server.listen(process.env.PORT || 3000, () => {
	discover();
	console.log(`API Gateway running on port ${process.env.PORT || 3000}`);
});