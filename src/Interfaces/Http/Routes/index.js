import TestHandler from "./../Handlers/test";
import ClassHandler from "./../Handlers/classes";
import MaterialHandler from "./../Handlers/material";
import AdminMiddleware from "./../../../Application/Middlewares/AdminMiddleware";
import StudentMiddleware from "./../../../Application/Middlewares/StudentMiddleware";
import TeacherMiddleware from "./../../../Application/Middlewares/TeacherMiddleware";

const RouterInstance = require('restify-router').Router;
const router = new RouterInstance();

router.get("/", (req, res) => {
	res.json({
		name: "auth service"
	});
});

/**
 * Class routers
 */
router.get("/admin/class*", [AdminMiddleware, ClassHandler]);
router.put("/admin/class*", [AdminMiddleware, ClassHandler]);
router.post("/admin/class*", [AdminMiddleware, ClassHandler]);

router.get("/student/class*", [StudentMiddleware, ClassHandler]);
router.post("/student/class*", [StudentMiddleware, ClassHandler]);

router.add("/teacher", require("./Teacher"));
router.get("/teacher/class*", [TeacherMiddleware, ClassHandler]);
router.put("/teacher/class*", [TeacherMiddleware, ClassHandler]);
router.post("/teacher/class*", [TeacherMiddleware, ClassHandler]);

/**
 * Test routers
 */
router.get("/admin/test*", [AdminMiddleware, TestHandler]);
router.put("/admin/test*", [AdminMiddleware, TestHandler]);
router.post("/admin/test*", [AdminMiddleware, TestHandler]);

router.get("/student/test*", [StudentMiddleware, TestHandler]);
router.put("/student/test*", [StudentMiddleware, TestHandler]);
router.post("/student/test*", [StudentMiddleware, TestHandler]);

router.get("/teacher/test*", [TeacherMiddleware, TestHandler]);
router.put("/teacher/test*", [TeacherMiddleware, TestHandler]);
router.post("/teacher/test*", [TeacherMiddleware, TestHandler]);

router.get("/teacher/question*", [TeacherMiddleware, TestHandler]);
router.put("/teacher/question*", [TeacherMiddleware, TestHandler]);
router.post("/teacher/question*", [TeacherMiddleware, TestHandler]);

/**
 * Material routers
 */
router.get("/admin/material*", [AdminMiddleware, MaterialHandler]);
router.put("/admin/material*", [AdminMiddleware, MaterialHandler]);
router.post("/admin/material*", [AdminMiddleware, MaterialHandler]);

router.get("/student/material*", [StudentMiddleware, MaterialHandler]);
router.put("/student/material*", [StudentMiddleware, MaterialHandler]);
router.post("/student/material*", [StudentMiddleware, MaterialHandler]);

router.get("/teacher/material*", [TeacherMiddleware, MaterialHandler]);
router.put("/teacher/material*", [TeacherMiddleware, MaterialHandler]);
router.post("/teacher/material*", [TeacherMiddleware, MaterialHandler]);

router.add("/auth", require("./User"));
// router.add("/student", require("./Student"));

module.exports = router;