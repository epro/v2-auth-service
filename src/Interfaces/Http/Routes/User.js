import { UserLoginService } from "./../../../Domain/User/Services/UserLoginService";
import { UserRegistrationService } from "./../../../Domain/User/Services/UserRegistrationService";
import { UserNotFoundException } from "./../../../Application/Exceptions/UserNotFoundException";

import SuccessResponse from "./../Responses/SuccessResponse";
import UnauthorizedResponse from "./../Responses/UnauthorizedResponse";
import InternalServerErrorResponse from "./../Responses/InternalServerErrorResponse";

const RouterInstance = require('restify-router').Router;
const router = new RouterInstance();

router.post("/login", async (req, res) => {
	try {
		const result = await new UserLoginService(req.body.username, req.body.password).attempt();
		return SuccessResponse(res, "Successfully logged in!", result);
	} catch (exception) {
		console.log(exception);
		if (exception instanceof UserNotFoundException) {
			return UnauthorizedResponse(res, exception.message);
		} else {
			return InternalServerErrorResponse(res, "An error occurred while logging in!");
		}
	}
});

router.post("/register", async (req, res) => {
	try {
		const result = await new UserRegistrationService(req.body).register();
		return SuccessResponse(res, "Successfully registered!", result);
	} catch (exception) {
		console.log(exception);
		return InternalServerErrorResponse(res, "An error occurred while registration!");
	}
});

module.exports = router;