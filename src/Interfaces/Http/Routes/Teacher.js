import TeacherController from "@controllers/TeacherController";

const RouterInstance = require('restify-router').Router;
const router = new RouterInstance();

router.get("/", TeacherController.index);
router.get("/:id", TeacherController.show);
router.put("/:id", TeacherController.update);
router.del("/:id", TeacherController.destroy);

module.exports = router;