/**
 * Internal server response builder
 * @param res
 * @param message
 * @param error_code
 * @returns {{success: boolean, message: *, error_code: *, data: null}}
 */
export default (res, message, error_code = 5) => {
    res.status(500);
    res.json({
        success: false,
        message: message,
        error_code: error_code,
        data: null
    });
}