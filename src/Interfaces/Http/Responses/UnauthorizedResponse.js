/**
 * Success response builder
 * @param res
 * @param message
 * @param data
 * @returns {{success: boolean, message: *, error_code: null, data: *}}
 */
export default (res, message, error_code = 4) => {
    res.status(401);
    res.json({
        success: false,
        message: message,
        error_code: error_code,
        data: null
    });
}