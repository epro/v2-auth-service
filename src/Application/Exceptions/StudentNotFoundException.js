export class StudentNotFoundException extends Error {

	constructor(message) {
		super(message);
		Error.captureStackTrace(this, StudentNotFoundException);
		this._message = message;
	}

	get message() {
		return this._message;
	}
}