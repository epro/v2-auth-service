export class TeacherNotFoundException extends Error {

	constructor(message) {
		super(message);
		Error.captureStackTrace(this, TeacherNotFoundException);
		this._message = message;
	}

	get message() {
		return this._message;
	}
}