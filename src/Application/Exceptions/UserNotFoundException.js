export class UserNotFoundException {

	constructor(message) {
		this.message = message;
	}
}