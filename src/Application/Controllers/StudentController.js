import { StudentNotFoundException } from "@exceptions/StudentNotFoundException";
import { SuccessResponse, NotFoundResponse, InternalServerErrorResponse } from "@responses";
import { CreateStudentService } from "@student/Services/CreateStudentService";
import { UpdateStudentService } from "@student/Services/UpdateStudentService";
import { DeleteStudentService } from "@student/Services/DeleteStudentService";
import { StudentRepository } from "@repositories/StudentRepository";

const students = {};

students.index = async (req, res) => {
	try {
		const result = await new StudentRepository().list(req.query.page, req.query.per_page).fetch(students => { return students.get() });
		return SuccessResponse(res, "List students", result);
	} catch (exception) {
		console.log(exception);
		return InternalServerErrorResponse(res, "An error occurred while gettng student data!");
	}
};

students.show = async (req, res) => {
	try {
		const result = await new StudentRepository().detail(req.params.id).then(student => { return student.get() });
		return SuccessResponse(res, "Detail student", result);
	} catch (exception) {
		console.log(exception);
		if (exception instanceof StudentNotFoundException) {
			return NotFoundResponse(res, exception.message);
		} else {
			return InternalServerErrorResponse(res, "An error occurred while gettng student data!");
		}
	}
};

students.store = async (req, res) => {
	try {
		const result = await new CreateStudentService(req.body.name, req.body.email, req.body.phone).persist();
		return SuccessResponse(res, "Successfully create new student!", result);
	} catch (exception) {
		console.log(exception);
		return InternalServerErrorResponse(res, "An error occurred while creating new student!");
	}
};

students.update = async (req, res) => {
	try {
		const result = await new UpdateStudentService(req.params.id, req.body).persist();
		return SuccessResponse(res, "Successfully update student data!", result);
	} catch (exception) {
		console.log(exception);
		if (exception instanceof StudentNotFoundException) {
			return NotFoundResponse(res, exception.message);
		} else {
			return InternalServerErrorResponse(res, "An error occurred while updating student data!");
		}
	}
};

students.destroy = async (req, res) => {
	try {
		const result = await new DeleteStudentService(req.params.id).persist();
		return SuccessResponse(res, "Successfully delete student data!", result);
	} catch (exception) {
		console.log(exception);
		if (exception instanceof StudentNotFoundException) {
			return NotFoundResponse(res, exception.message);
		} else {
			return InternalServerErrorResponse(res, "An error occurred while deleting student data!");
		}
	}
};

module.exports = students;