import { TeacherRepository } from "@repositories/TeacherRepository";
import { TeacherNotFoundException } from "@exceptions/TeacherNotFoundException";
import { SuccessResponse, NotFoundResponse, InternalServerErrorResponse } from "@responses";
import { CreateTeacherService } from "@teacher/Services/CreateTeacherService";
import { UpdateTeacherService } from "@teacher/Services/UpdateTeacherService";
import { DeleteTeacherService } from "@teacher/Services/DeleteTeacherService";

const teachers = {};

teachers.index = async (req, res) => {
	try {
		let data = [];
		if (req.query.fetch === "all" || req.query.q !== null) data = await new TeacherRepository().all(req.query.q);
		return SuccessResponse(res, "List teachers", data);
	} catch (exception) {
		console.log(exception);
		return InternalServerErrorResponse(res, "An error occurred while getting teacher data!");
	}
};

teachers.show = async (req, res) => {
	try {
		const teacher = await new TeacherRepository().findById(req.params.id).then(result => { return result.get() });
		return SuccessResponse(res, "Detail teacher", teacher);
	} catch (exception) {
		console.log(exception);
		if (exception instanceof TeacherNotFoundException) {
			return NotFoundResponse(res, exception.message);
		} else {
			return InternalServerErrorResponse(res, "An error occurred while getting teacher detail!");
		}
	}
};

teachers.store = async (req, res) => {
	try {
		const result = await new CreateTeacherService(req.body.name, req.body.email, req.body.phone).persist();
		return SuccessResponse(res, "Successfully create new teacher data!", result);
	} catch (exception) {
		console.log(exception);
		return InternalServerErrorResponse(res, "An error occurred while creating new teacher!");
	}
};

teachers.update = async (req, res) => {
	try {
		const result = await new UpdateTeacherService(req.params.id, req.body).persist();
		return SuccessResponse(res, "Successfully update teacher data!", result);
	} catch (exception) {
		console.log(exception);
		if (exception instanceof TeacherNotFoundException) {
			return NotFoundResponse(res, exception.message);
		} else {
			return InternalServerErrorResponse(res, "An error occurred while updating teacher data!");
		}
	}
};

teachers.destroy = async (req, res) => {
	try {
		const result = await new DeleteTeacherService(req.params.id).persist();
		return SuccessResponse(res, "Successfully delete teacher data!", null);
	} catch (exception) {
		console.log(exception);
		if (exception instanceof TeacherNotFoundException) {
			return NotFoundResponse(res, exception.message);
		} else {
			return InternalServerErrorResponse(res, "An error occurred while deleting teacher data!");
		}
	}
};

module.exports = teachers;