import mongoose from "mongoose";

const Schema = mongoose.Schema;

/**
 * Defining user schema
 */
const user = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    username: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    role: {
        type: String,
        required: true,
        enum: ["ADMIN", "TEACHER", "STUDENT"]
    },
    student: {
        type: Schema.Types.ObjectId,
        default: null,
        ref: "Student"
    },
    teacher: {
        type: Schema.Types.ObjectId,
        default: null,
        ref: "Teacher"
    },
    created_at: {
        type: Date,
        default: Date.now()
    },
    updated_at: {
        type: Date,
        default: Date.now()
    },
    deleted_at: Date
});

/**
 * Creating user model
 * @type {Model}
 */
const User = mongoose.model("User", user);

/**
 * Exporting user model
 * @type {Model}
 */
module.exports = User;