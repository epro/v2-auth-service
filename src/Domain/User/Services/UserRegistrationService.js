import UserEntity from "./../Entities/UserEntity";
import UserLoginEntity from "./../Entities/UserLoginEntity";
import { TokenGenerator } from "./../../../Infrastructure/TokenGenerator";
import { CreateStudentService } from "./../../Student/Services/CreateStudentService";
import { CreateTeacherService } from "./../../Teacher/Services/CreateTeacherService";

export class UserRegistrationService {

	constructor (data) {
		this._data = data;
	}

	async register () {
		if (this._data.role === "STUDENT") this._data.student = await new CreateStudentService(this._data.name, this._data.email, this._data.phone).persist();
		if (this._data.role === "TEACHER") this._data.teacher = await new CreateTeacherService(this._data.name, this._data.email, this._data.phone).persist();

		const user = await this.storeUser();
		const token = new TokenGenerator().generate({
            "id": user._id,
            "name": user.name,
            "role": user.role
        });

        return  {
        	id: user._id,
            name: user.name,
            email: this._data.email,
            phone: this._data.phone,
            username: user.username,
            role: this._data.role,
            token: token
        };
	}

	async storeUser() {
		return UserEntity.create(this._data).then(user => {
			return user;
		}).catch(error => {
			throw error;
		});
	}
}