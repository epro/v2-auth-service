import UserLoginEntity from "./../Entities/UserLoginEntity";
import { TokenGenerator } from "./../../../Infrastructure/TokenGenerator";
import { UserRepository } from "./../../../Infrastructure/Repositories/UserRepository";

export class UserLoginService {

	constructor(username, password) {
		this._username = username;
		this._password = password;
	}

	async attempt() {
		const user = await new UserRepository().findByCredential(this._username, this._password);
		const token = new TokenGenerator().generate({
            "id": user._id,
            "reference_id": (user[user.role.toLowerCase()] !== undefined) ? user[user.role.toLowerCase()]._id : null,
            "name": user.name,
            "role": user.role
        });

        this.storeToken(user._id, token);
       	return {
       		id: user._id,
	        name: user.name,
	        email: (user[user.role.toLowerCase()] !== undefined) ? user[user.role.toLowerCase()].email : null,
	        phone: (user[user.role.toLowerCase()] !== undefined) ? user[user.role.toLowerCase()].phone : null,
	        username: user.username,
	        role: user.role,
	        token: token
       	};
	}

	storeToken(user_id, token) {
		UserLoginEntity.update(
            { user_id: user_id },
            { user_id: user_id, token: token },
            { upsert: true, setDefaultsOnInsert: true }
        );
	}
}