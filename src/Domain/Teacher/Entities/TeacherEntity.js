import mongoose from "mongoose";

/**
 * Defining teacher schema
 */
const teacher = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    phone: {
        type: String,
        unique: true,
        required: true
    },
    profile_image: String,
    created_at: {
        type: Date,
        default: Date.now()
    },
    updated_at: {
        type: Date,
        default: Date.now()
    },
    deleted_at: Date
});

/**
 * Creating teacher model
 * @type {Model}
 */
const Teacher = mongoose.model("Teacher", teacher);

/**
 * Exporting teacher model
 * @type {Model}
 */
module.exports = Teacher;