import TeacherEntity from "@teacher/Entities/TeacherEntity";

export class CreateTeacherService {

	constructor (name, email, phone) {
		this._name = name;
		this._email = email;
		this._phone = phone;
	}

	async persist () {
		return TeacherEntity.create({
			name: this._name,
			email: this._email,
			phone: this._phone
		}).then(teacher => {
			return teacher;
		}).catch(error => {
			throw error;
		});
	}
}