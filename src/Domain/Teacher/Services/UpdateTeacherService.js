import UserEntity from "@user/Entities/UserEntity";
import TeacherEntity from "@teacher/Entities/TeacherEntity";
import TeacherNotFoundException from "@exceptions/TeacherNotFoundException";

export class UpdateTeacherService {

	constructor(id, data) {
		this._id = id;
		this._data = data;
	}

	async persist() {
		return TeacherEntity.findByIdAndUpdate(this._id, {
			$set: this._data
		}).then(async updated => {
			if (updated !== null) {
				await UserEntity.update({ teacher: this._id }, { name: this._data.name });
				return updated;
			}

			throw new TeacherNotFoundException("The teacher data you're looking for couldn't be found!");
		}).catch(error => {
			throw error;
		});
	}
}