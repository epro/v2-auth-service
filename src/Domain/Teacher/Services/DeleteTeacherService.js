import UserEntity from "@user/Entities/UserEntity";
import TeacherEntity from "@teacher/Entities/TeacherEntity";

export class DeleteTeacherService {

	constructor(id) {
		this._id = id;
	}

	async persist() {
		return TeacherEntity.findByIdAndUpdate(this._id, {
			$set: {
				deleted_at: Date.now()
			}
		}).then(async updated => {
			if (updated !== null) {
				await UserEntity.updateOne({ teacher: this._id }, { deleted_at: Date.now() });
				return updated;
			}

			throw new TeacherNotFoundException("The teacher you're looking for couldn't be found!");
		}).catch(error => {
			throw error;
		});
	}
}