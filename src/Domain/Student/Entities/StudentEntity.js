import mongoose from "mongoose";

/**
 * Defining student schema
 */
const student = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    phone: {
        type: String,
        unique: true,
        required: true
    },
    profile_picture: String,
    created_at: {
        type: Date,
        default: Date.now()
    },
    updated_at: {
        type: Date,
        default: Date.now()
    },
    deleted_at: Date
});

/**
 * Creating student model
 * @type {Model}
 */
const Student = mongoose.model("Student", student);

/**
 * Exporting student model
 * @type {Model}
 */
module.exports = Student;