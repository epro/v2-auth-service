import StudentEntity from "./../Entities/StudentEntity";
import StudentNotFoundException form "./../../../Application/Exceptions/StudentNotFoundException";

export class UpdateStudentService {

	async persist(id, data) {
		return StudentEntity.findByIdAndUpdate(id, {
			$set: data
		}, {
			new: true
		}).then(updated => {
			if (updated !== null) {
				return updated;
			}

			throw new StudentNotFoundException("The student you're looking for couldn't be found!");
		}).catch(error => {
			throw error;
		});
	}	
}