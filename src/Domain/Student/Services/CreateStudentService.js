import StudentEntity from "@student/Entities/StudentEntity";

export class CreateStudentService {

	constructor (name, email, phone) {
		this._name = name;
		this._email = email;
		this._phone = phone;
	}

	async persist () {
		return StudentEntity.create({
			name: this._name,
			email: this._email,
			phone: this._phone
		}).then(student => {
			return student;
		}).catch (error => {
			throw error;
		});
	}
}