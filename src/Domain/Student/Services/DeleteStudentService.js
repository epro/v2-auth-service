import StudentEntity from "./../Entities/StudentEntity";

export class DeleteStudentService {

	constructor(id) {
		this._id = id;
	}

	async persist() {
		return StudentEntity.findByIdAndUpdate(id, {
			$set: {
				deleted_at: Date.now()
			}
		}).then(updated => {
			if (updated !== null) {
				return updated;
			}

			throw new StudentNotFoundException("The student you're looking for couldn't be found!");
		}).catch(error => {
			throw error;
		});
	}
}