import TeacherEntity from "@teacher/Entities/TeacherEntity";
import { InvalidStateException } from "@exceptions/InvalidStateException";
import { TeacherNotFoundException } from "@exceptions/TeacherNotFoundException";

export class TeacherRepository {

	async all (search_key = null) {
		let params = {
			deleted_at: null
		};
		
		if (search_key !== null) params.name = new RegExp(search_key.toLowerCase(), "ig");
		return TeacherEntity.find(params).select("_id name email phone").lean();
	}

	list (page = 1, per_page = 10) {
		this._state = "LIST";
		this._page = page;
		this._per_page = per_page;
		return this;
	}

	async fetch () {
		this._teachers = await TeacherEntity.find(params).select("_id name email phone").lean();
		return this;
	}

	async findById(id) {
		this._state = "DETAIL";
		this._teacher = await TeacherEntity.findById(id).lean();

		if (this._teacher !== null) {
			return this;
		} else {
			throw new TeacherNotFoundException("The teacher you're looking for couldn't be found!");
		}
	}

	get () {
		switch (this._state) {
			case "LIST":
				return this._teachers;
			case "DETAIL":
				return this._teacher;
			default:
				throw new InvalidStateException("Invalis state!");
		}
	}
}