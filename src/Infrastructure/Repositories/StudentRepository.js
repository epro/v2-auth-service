import StudentEntity from "@student/Entities/StudentEntity";
import { InvalidStateException } from "@exceptions/InvalidStateException";
import { StudentNotFoundException } from "@exceptions/StudentNotFoundException";

export class StudentRepository {

	list (page, per_page) {
		this._page = page;
		this._per_page = per_page;
		this._state = "LIST";
		return this;
	}

	async fetch () {
		this._total_students = await StudentEntity.find({ deleted_at: null }).count();
		this._students = await StudentEntity.find({ deleted_at: null }).skip(this._per_page * (this._page - 1)).limit(this._per_page).lean();
		return this;
	}

	async detail (id) {
		this._state = "DETAIL";
		this._student = StudentEntity.findById(id).lean();
		if (this._student !== null) {
			return this;
		} else {
			throw new StudentNotFoundException("The student you're looking for couldn't be found!");
		}
	}

	get () {
		switch (this._state) {
			case: "LIST":
				return {
					total: this._total_students,
					current_page: this._page,
					per_page: this._per_page,
					data: this._students
				};
			case: "DETAIL":
				return this._student;
			default:
				throw new InvalidStateException("Invalid state!");
		}
	}
}