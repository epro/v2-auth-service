import UserEntity from "./../../Domain/User/Entities/UserEntity";
import { UserNotFoundException } from "./../../Application/Exceptions/UserNotFoundException";

export class UserRepository {
    
    /**
     * Find user by credential
     * 
     * @param string username 
     * @param string password
     */
    async findByCredential(username, password) {
        return UserEntity.findOne({
            username: username,
            password: password
        }).populate("teacher").populate("student").then(user => {
            if (user !== null) {
                return user;
            } else {
                throw new UserNotFoundException("Invalid username or password!");
            }
        }).catch(errUser => {
            throw errUser;
        });
    }
}