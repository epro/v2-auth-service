import { UserLoginService } from "./../../../src/Domain/User/Services/UserLoginService";
import { UserNotFoundException } from "./../../../src/Application/Exceptions/UserNotFoundException";
import should from "should";
import assert from "assert";

describe("USER LOGIN SERVICE TEST", () => {

	before(done => {
		require("./../../DBConnection");
		done();
	});

	const credentials = {
		admin: {
			username: "admin",
			password: "rahasia123"
		},
		student: {
			username: "student1",
			password: "rahasia123"
		},
		teacher: {
			username: "teacher1",
			password: "rahasia123"
		}
	};

	let result = "";
	describe("# ADMIN LOGIN", () => {
		before(done => {
			new UserLoginService(credentials.admin.username, credentials.admin.password).attempt().then(user => {
				result = user;
				done();
			});
		});

		it("Should return result as an object", done => {
            should(result).be.an.Object();
            done();
        });

        it("Should return a valid response body", done => {
        	should(result).have.property("id");
            should(result).have.property("name").which.is.a.String();
            should(result).have.property("username").which.is.a.String();
            should(result).have.property("role").equal("ADMIN");
            should(result).have.property("token").which.is.a.String();
            done();
        });

        describe("Negative tests", () => {
        	let error = "";
        	before(done => {
        		new UserLoginService(credentials.admin.username, "blablabla").attempt().catch(exception => {
        			error = exception;
        			done();
        		});
        	});

        	it("Should throw UserNotFoundException if the username or password is not valid", done => {
	        	assert.throws(() => {
	        		throw new UserNotFoundException("Invalid username or password!");
	        	}, error);
	        	done();
	        });

	        it("Should have 'Invalid username or password!' error message", done => {
	        	should(error.message).equal("Invalid username or password!");
	        	done();
	        });
        });
	});

	describe("# STUDENT LOGIN", () => {
		before(done => {
			new UserLoginService(credentials.student.username, credentials.student.password).attempt().then(user => {
				result = user;
				done();
			});
		});

		it("Should return result as an object", done => {
            should(result).be.an.Object();
            done();
        });

        it("Should return a valid response body", done => {
        	should(result).have.property("id");
            should(result).have.property("name").which.is.a.String();
            should(result).have.property("username").which.is.a.String();
            should(result).have.property("email").which.is.a.String();
            should(result).have.property("phone").which.is.a.String();
            should(result).have.property("role").equal("STUDENT");
            should(result).have.property("token").which.is.a.String();
            done();
        });

        describe("Negative tests", () => {
        	let error = "";
        	before(done => {
        		new UserLoginService(credentials.student.username, "blablabla").attempt().catch(exception => {
        			error = exception;
        			done();
        		});
        	});

        	it("Should throw UserNotFoundException if the username or password is not valid", done => {
	        	assert.throws(() => {
	        		throw new UserNotFoundException("Invalid username or password!");
	        	}, error);
	        	done();
	        });

	        it("Should have 'Invalid username or password!' error message", done => {
	        	should(error.message).equal("Invalid username or password!");
	        	done();
	        });
        });
	});
});