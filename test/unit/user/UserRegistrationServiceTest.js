import { UserRegistrationService } from "./../../../src/Domain/User/Services/UserRegistrationService";
import { ValidationError } from "mongoose";
import StudentEntity from "./../../../src/Domain/Student/Entities/StudentEntity";
import TeacherEntity from "./../../../src/Domain/Teacher/Entities/TeacherEntity";
import UserEntity from "./../../../src/Domain/User/Entities/UserEntity";
import should from "should";
import assert from "assert";

describe("USER REGISTRATION TEST", () => {
	let admin_count = 0;
	let userData = {};
	let student_count = 0;
	let teacher_count = 0;

	before(done => {
		require("./../../DBConnection");
		UserEntity.countDocuments({role: "ADMIN"}).then(admin => {
			admin_count = admin++;
			userData.admin = {
				name: `ADMIN ${admin_count}`,
				role: "ADMIN",
				username: `admin${admin_count}`,
				password: "rahasia123",
			};
		}).then(() => {
			UserEntity.countDocuments({role: "STUDENT"}).then(student => {
				student_count = student++;
				userData.student = {
					name: `STUDENT ${student_count}`,
					role: "STUDENT",
					username: `student${student_count}`,
					phone: `0812345678${student_count}`,
					password: "rahasia123",
					email: `student${student_count}@mail.com`
				};
			});
		}).then(() => {
			UserEntity.countDocuments({role: "TEACHER"}).then(teacher => {
				teacher_count = teacher++;
				userData.teacher = {
					name: `TEACHER ${teacher_count}`,
					role: "TEACHER",
					username: `teacher${teacher_count}`,
					phone: `0912345678${teacher_count}`,
					password: "rahasia123",
					email: `teacher${teacher_count}@mail.com`
				};
			});

			done();
		});
	});

	let result = "";

	/**
	 * Register admin
	 */
	describe("# REGISTER ADMIN", () => {
		before(done => {
			new UserRegistrationService(userData.admin).register().then(user => {
				result = user;
				admin_count++;
				userData.admin.name = `ADMIN ${admin_count}`;
				userData.admin.username = `admin${admin_count}`;
				done();
			});
		});

		it("Should return result as an object", done => {
            should(result).be.an.Object();
            done();
        });

        it("Should return a valid response body", done => {
        	should(result).have.property("id");
            should(result).have.property("name").which.is.a.String();
            should(result).have.property("username").which.is.a.String();
            should(result).have.property("role").equal("ADMIN");
            should(result).have.property("token").which.is.a.String();
            done();
        });

        describe("Negative tests", () => {
        	let error = "";
        	before(done => {
        		delete userData.admin.name;
        		new UserRegistrationService(userData.admin).register().catch(exception => {
					error = exception;
					done();
				});
        	});

        	it("Should throw error if the data is not valid", done => {
	        	should(error.name).equal("ValidationError");
	        	done();
	        });
        });
	});

	/**
	 * Register student
	 */
	describe("# REGISTER STUDENT", () => {
		before(done => {
			new UserRegistrationService(userData.student).register().then(user => {
				result = user;
				student_count++;
				userData.student.name = `STUDENT ${student_count}`;
				userData.student.username = `student${student_count}`;
				userData.student.phone = `0812345678${student_count}`;
				userData.student.email = `student${student_count}@mail.com`;
				done();
			});
		});

		it("Should return result as an object", done => {
            should(result).be.an.Object();
            done();
        });

        it("Should return a valid response body", done => {
        	should(result).have.property("id");
            should(result).have.property("name").which.is.a.String();
            should(result).have.property("username").which.is.a.String();
            should(result).have.property("email").which.is.a.String();
            should(result).have.property("phone").which.is.a.String();
            should(result).have.property("role").equal("STUDENT");
            should(result).have.property("token").which.is.a.String();
            done();
        });

        describe("Negative tests", () => {
        	let error = "";
        	before(done => {
        		delete userData.student.name;
        		new UserRegistrationService(userData.student).register().catch(exception => {
					error = exception;
					done();
				});
        	});

        	it("Should throw error if the data is not valid", done => {
	        	should(error.name).equal("ValidationError");
	        	done();
	        });
        });
	});

	/**
	 * Register teacher
	 */
	describe("# REGISTER TEACHER", () => {
		before(done => {
			new UserRegistrationService(userData.teacher).register().then(user => {
				result = user;
				teacher_count++;
				userData.teacher.name = `TEACHER ${teacher_count}`;
				userData.teacher.username = `teacher${teacher_count}`;
				userData.teacher.phone = `0812345678${teacher_count}`;
				userData.teacher.email = `teacher${teacher_count}@mail.com`;
				done();
			});
		});

		it("Should return result as an object", done => {
            should(result).be.an.Object();
            done();
        });

        it("Should return a valid response body", done => {
        	should(result).have.property("id");
            should(result).have.property("name").which.is.a.String();
            should(result).have.property("username").which.is.a.String();
            should(result).have.property("email").which.is.a.String();
            should(result).have.property("phone").which.is.a.String();
            should(result).have.property("role").equal("TEACHER");
            should(result).have.property("token").which.is.a.String();
            done();
        });

        describe("Negative tests", () => {
        	let error = "";
        	before(done => {
        		delete userData.teacher.name;
        		new UserRegistrationService(userData.teacher).register().catch(exception => {
					error = exception;
					done();
				});
        	});

        	it("Should throw error if the data is not valid", done => {
	        	should(error.name).equal("ValidationError");
	        	done();
	        });
        });
	});
});
