import mongoose from "mongoose";

require('dotenv').config();

before(() => {
    mongoose.set('useFindAndModify', false);
    mongoose.connect(process.env.MONGO_URI, {
        useNewUrlParser: true,
	    useCreateIndex: true
    });

    require("./../src/Domain/Student/Entities/StudentEntity");
    require("./../src/Domain/Teacher/Entities/TeacherEntity");
});

after(() => {
    mongoose.disconnect();
});